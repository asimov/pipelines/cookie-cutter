import importlib

from asimov.pipeline import Pipeline
from asimov import logger

__version__ = 0.0.1 # Please use semantic versioning if you can

class MyPipeline(Pipeline):
    """
    The MyPipeline Pipeline integration for asimov.

    This integration allows asimov to create and monitor MyPipeline jobs.
    """

    name = mypipeline # This is the name of the pipeline used by asimov, for example in analysis specifications
    # The next line defines the location of the template configuration file if the pipeline requires it
    config_template = importlib.resources.path(__name__, 'mypipeline.ini')

    def __init__(self, analysis):
        super().__init__(analysis)

    def detect_completion(self):
        """
        Check for the production of a final posterior file to
        signal that a job has been completed.
        """
        pass

    def build_dag(self):
        """
        Construct a DAG file for the analysis to be passed to ``my_inference``.
        """
        pass

    def submit_dag(self):
        """
        Submit the DAG file to the cluster.
        """
        pass

    def collect_assets(self):
        """
        Collect all of the output assets for this job.
        """
        pass

    def samples(self):
        """
        Collect the combined samples file for PESummary.
        """
        pass

    def after_completion(self):
        """
        A hook to be run after the pipeline is detected to have completed.
        """
        pass

    def collect_logs(self):
        """
        Collect all of the log files produced by this pipeline and return their contents as a dictionary.
        """
        pass

    def check_progress(self):
        """
        Return the progress of this job.
        """
        pass

    def read_ini(self):
        """
        Read and parse a configuration file for this pipeline.
        """
        pass

    def resurrect(self):
        """
        Attempt to fix and restart a stuck or failed job.
        """
        pass
