# Asimov pipeline cookie-cutter

This is a cookie-cutter repository for setting up your own pipeline as an asimov plugin.
This repository can be used as a template for setting-up an independent python package which works to integrate another pipeline with asimov, however it is also possible to add code to an existing package to allow it to be discoverable by asimov.

If you're the maintainer of a pipeline it may make better sense to take that approach than using this one.

